add = 3 + 5
sub = 5 - 2
times = 5 * 4
modulo = 9 % 2
expo = 2 ** 3

# you need to specify explicitly that you want Python to use the integer
# as a string of characters. You can do this by wrapping the variable in the
# str() function, which tells Python to represent non-string values as strings

print("Addition: " + str(add) + "\n")
print("Subtraction: " + str(sub) + "\n")
print("Multiplication: " + str(times) + "\n")
print("Modulo: " + str(modulo) + "\n")
print("Exponentiation: " + str(expo) + "\n")