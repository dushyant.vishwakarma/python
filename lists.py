# A list is a collection of items in a particular order. You can make a list that
# includes the letters of the alphabet, the digits from 0 to 9, or the names of
# all the people in your family.

# In Python, square brackets ([]) indicate a list, and individual elements
# in the list are separated by commas.

phones = ['iphone 6','samsung galaxy note 10','Oneplus 7','Nokia 8']
print(phones)
print(phones[0])
print(phones[1].title())

# Python has a special syntax for accessing the last element in a list. By asking
# for the item at index -1, Python always returns the last item in the list:

print(phones[-1])

# The index -2 returns the second item from the end of the list,
# -2
# the index -3 returns the third item from the end, and so forth.
# 3

print(phones[-2])

# You can use individual values from a list just as you would any other vari
# able.
# For example, you can use concatenation to create a message based on
# a value from a list.

message = "I wish to buy " + phones[3] + " someday."
print(message) 

# The syntax for modifying an element is similar to the syntax for accessing
# an element in a list. To change an element, use the name of the list followed
# by the index of the element you want to change, and then provide the new
# value you want that item to have.

phones[0] = "iphone XS"
print(phones[0])

# The simplest way to add a new element to a list is to append the item to the
# list. When you append an item to a list, the new element is added to the end
# of the list.

phones.append('Redmi 4')
print(phones)

# You can add a new element at any position in your list by using the
# insert()
# method. You do this by specifying the index of the new element and the
# value of the new item.

phones.insert(3,'Samsung Galaxy S10')
print(phones)

# If you know the position of the item you want to remove from a list, you can
# use the
# statement.
# del

del phones[0]
print(phones)

# The
# method removes the last item in a list, but it lets you work
# pop()
# with that item after removing it. The term pop comes from thinking of a
# list as a stack of items and popping one item off the top of the stack. In
# this analogy, the top of a stack corresponds to the end of a list.

popped_phone = phones.pop()
print(phones)
print(popped_phone)

# You can actually use pop() to remove an item in a list at any position by
# including the index of the item you want to remove in parentheses.

first_phone = phones.pop(0)
print(phones)

# Sometimes you wont know the position of the value you want to remove
# from a list. If you only know the value of the item you want to remove, you
# can use the remove() method.

phones.remove('Nokia 8')
print(phones)


