name = "ada lovelace"
print(name.title())
print(name.upper())
print(name.lower())

first_name = "Eric"
last_name = "James"
full_name = first_name + " " + last_name
# print(first_name + " " +last_name)
print("Hello " + full_name.title() + "!")