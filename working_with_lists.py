# Looping allows you to take the same action, or set of actions,
# with every item in a list. As a result, you’ll be able to work efficiently with
# lists of any length, including those with thousands or even millions of items.

# Let’s use a for loop to print out each name in a list of magicians:

magicians = ['Dynamo','Carolina','David','Angelo']
for magician in magicians:
    print(magician.title() + ", that was a great trick.")
    print("I can't wait to see your next trick, " + magician.title() + ".\n")

print("Thank you, everyone. That was a great magic show!")

# When you’re using loops for the first time, keep in mind that the set of
# steps is repeated once for each item in the list, no matter how many items
# are in the list. If you have a million items in your list, Python repeats these
# steps a million times—and usually very quickly.

# Using singular and plural names can help
# you identify whether a section of code is working with a single element from
# the list or the entire list.

# You can also write as many lines of code as you like in the for loop.
# Every indented line following the line for magician in magicians is con-
# sidered inside the loop, and each indented line is executed once for each
# value in the list. Therefore, you can do as much work as you like with
# each value in the list.

# Python uses indentation to determine when one line of code is connected to
# the line above it. In the previous examples, the lines that printed messages to
# individual magicians were part of the for loop because they were indented.
# Python’s use of indentation makes code very easy to read. Basically, it uses
# whitespace to force you to write neatly formatted code with a clear visual
# structure. In longer Python programs, you’ll notice blocks of code indented
# at a few different levels. These indentation levels help you gain a general
# sense of the overall program’s organization.

# If you accidentally indent a line that doesn’t need to be indented, Python
# informs you about the unexpected indent:

# message = "Hello Python"
#     print(message)

# Python’s range() function makes it easy to generate a series of numbers.
# For example, you can use the range() function to print a series of numbers
# like this:

for value in range(1,10):
    print(value)

# The range() function causes Python to start counting at the first
# value you give it, and it stops when it reaches the second value you provide.
# Because it stops at that second value, the output never contains the end
# value, which would have been 10 in this case.

# If you want to make a list of numbers, you can convert the results of range()
# directly into a list using the list() function. When you wrap list() around a
# call to the range() function, the output will be a list of numbers.

print("\n")

nums = list(range(22,33))
print(nums)

# We can also use the range() function to tell Python to skip numbers
# in a given range. For example, here’s how we would list the even numbers
# between 1 and 10:

print("\n")

even_nums = list(range(2,11,2))
print(even_nums)

# A few Python functions are specific to lists of numbers. For example, you
# can easily find the minimum, maximum, and sum of a list of numbers:

print("\n")

digits = [1,2,3,4,5,6,7,8,9]

print("Minimum from digits: " + str(min(digits)) + "\n")
print("Maximum from digits: " + str(max(digits)) + "\n")
print("Sum of digits: " + str(sum(digits)) + "\n")

# A list comprehension allows you to generate
# this same list in just one line of code. A list comprehension combines the
# for loop and the creation of new elements into one line, and automatically
# appends each new element.

# The following example builds the same list of square numbers you saw
# earlier but uses a list comprehension:

print("\n")

squares = [value**2 for value in range(1,11)]
print(squares)