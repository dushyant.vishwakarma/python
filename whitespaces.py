print("\tPython\n")

print("Languages:\nPython\nJava\nPHP\nJavaScript")

# Python can look for extra whitespace on the right and left sides of a
# string. To ensure that no whitespace exists at the right end of a string, use
# the rstrip() method.

message = "     hello        "
print(message.strip())