# A function is a block of code which only runs when it is called. In python, we do not use curly brackets, we use indentation with tabs and spaces

# Create function
def sayHello(name='Bruce Wayne'):       # Default value provided
    print(f'Hello {name}')

sayHello('Arthur Fleck')
sayHello()

# Return values
def getSum(num1,num2):
    total = num1 + num2
    return total

print(getSum(4,7))

number = getSum(5,8)
print(number)

# A lambda function is a small anonymous function.
# A lambda function can take any number of arguments, but can only have one expression. A Lambda funtion
# is very similar to JS arrow functions

# Create lambda function
getSub = lambda num1,num2: num1 - num2

print(getSub(9,3))