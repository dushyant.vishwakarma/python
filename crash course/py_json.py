# JSON is commonly used with data APIs. Here is how we can parse JSON into a python dictionary

import json

# Sample JSON
userJSON = '{"first_name":"John","last_name":"Doe","age":23}'

# parse JSON to dict
user = json.loads(userJSON)

print(user)
print(user['first_name'])

# dict to JSON

car = {'make': 'Ford','model': 'Mustang','year':1970}
# convert dict to JSON
carJSON = json.dumps(car)

print(carJSON)