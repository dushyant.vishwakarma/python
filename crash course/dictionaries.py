# A Dictionary which is unordered, changeable and indexed. No duplicate members

# Create dict
person = {
    'first_name' : 'John',
    'last_name' : 'Doe',
    'age' : 23
}

# Use constructor
person2 = dict(first_name='Sarah',last_name='Williams')

# Get Value
print(person['first_name'])

# Get value using get value
print(person.get('last_name'))

# Add key/value
person['phone_number'] = '159-458-784'

# Get dict keys
print(person2.keys())

# Get dict items
print(person2.items())

print(person,type(person))
print(person2,type(person2))

# Copy dict
person3 = person.copy()
person3['city'] = 'Bhopal'
print(person3)

# Remove item
del(person['age'])
print(person)

# Remove with pop
person.pop('phone_number')
print(person)

# Clear dict
person.clear()
print(person)

# Get length
print(len(person2))

# List of dict
people = [
    {'name': 'Kevin', 'age': 24},
    {'name': 'Ben', 'age': 23}
]

print(people)

# Get a value from list of dict
print(people[1]['name'])