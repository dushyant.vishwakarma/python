# Strings in python are surrounded by either single quotes or double quotes
# String Formatting and some string methods

name = "Dushyant"
age = 23

# Concatenate 
# print("Hello, My name is " + name + " and my age is " + str(age))

# String Formatting 
# Arguments by position
# print('Hi,I am {name} and my age is {age}'.format(name=name, age=age))

# F-strings (Python 3.6+)
print(f'Hi, I am {name} and my age is {age}')

# string methods

s = 'hello world'

# String Capitalize
print(s.capitalize())

# Make all uppercase
print(s.upper())

# Make all lowercase
s2 = 'GOODBYE WORLD'
print(s2.lower())

# Swap case
print(s2.swapcase())

# get length
print(len(s2))

# Replace
print(s.replace('world','everyone'))

# Count
sub_str = 'h'
print(s.count(sub_str))

# Starts with
print(s.startswith('h'))

# Ends with
print(s.endswith('d'))

# Split into a list
print(s.split())

# Find position
print(s.find('r'))

# Is all alphanum
print(s.isalnum())

# Is all numeric
print(s.isnumeric())

# Is all aplhabetic
print(s.isalpha())