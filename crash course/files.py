# Python has function for creating, reading, updating, and deleting files.

# Open a file
# If the file does not exist, it will create it.
myFile = open('myfile.txt','w')     # w => write mode

# Get some info about the file opened
print("Name: "+myFile.name)
print("Mode: "+myFile.mode)
print("Is closed: "+str(myFile.closed))

# Write to file
myFile.write("I love the following programming languages:-\n ")
myFile.write("Python, PHP, JavaScript")
myFile.close()

# Append to file
myFile = open('myfile.txt','a')     # a => append mode
myFile.write('\nI also like Java')
myFile.close()

# Read from file
myFile = open('myfile.txt','r+')
text = myFile.read(100)
print(text)