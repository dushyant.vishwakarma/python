# If/Else conditions are used to execute code based on some condition being true or false.

x = 32
y = 10

# Comparison operators ( == , !=, >, <, >=, <=) - Used to compare values

# If statement
if x > y:
    print(f'{x} is greater than {y}')

# If/Else statement
if x > y:
    print(f'{x} is greater than {y}')
# elif
elif x == y:
    print(f'{x} is equal to {y}')
else:
    print(f'{y} is greater than {x}')

# Nested If
if x > 2:
    if x <=10:
        print(f'{x} is greater than 2 and less than or equal to 10')

# Logical operators (and,or,not) - used to combine conditional statements
# and
if y > 2 and y <= 10:
    print(f'{y} is greater than 2 and less than or equal to 10')

# or
if y > 2 or y > 10:
    print(f'{y} is greater than 2 and less than or equal to 10')

# not
if x != 10:
    print(f'{x} is not equal to 10')


# Membership operators (not, not in) - Membership operators are used to test if a sequence is presented in an object

# in
numbers = [1,2,3,4,5]
if x in numbers:
    print(x in numbers) 

# not in
numbers = [1,2,3,4,5]
if x not in numbers:
    print(x not in numbers) 

# Identity operators (is, is not) - Compare the objects, not if they are equal, but if they are
# actually the same object , with the same memory location:

# is
if x is y:
    print(x is y)
else:
    print('x is not y')

# is not 
if x is not y:
    print(x is not y)
