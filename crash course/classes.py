# A class is like a blueprint for creating objects. An object has properties and methods (functions) associated with it. Almost everything in python is an object.

 # Create a class

class User:
    # Constructor
    def __init__(self,name,email,age):
        self.name = name
        self.email = email
        self.age = age

    def greeting(self):
        return f'Hi my name is {self.name} and I am {self.age} years old.'
    
    def has_birthday(self):
        self.age += 1

# Extending Class
class Customer(User):
    # Constructor
    def __init__(self,name,email,age):
        self.name = name
        self.email = email
        self.age = age
        self.balance = 0

    def set_balance(self,balance):
        self.balance = balance

    def greeting(self):
        return f'Hi my name is {self.name} and I am {self.age} years old and my balance is {self.balance}'


# Initiate User object
john = User('John','john@test.com',23)

# initiate customer object
jane_customer = Customer('Jane Doe','jane@test.com',23)

# set balance of customer object
jane_customer.set_balance(5000)

# print(type(john))

# Accessing the objects properties
print(john.name + " " + john.email + " " + str(john.age))

# Accessing the objects methods
john.has_birthday()
print(john.greeting())

# Accessing parent class method
# print(jane_customer.greeting())

# Overwriting parent class method
print(jane_customer.greeting())
