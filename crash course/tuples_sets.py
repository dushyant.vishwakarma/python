# A Tuple is a collection which is ordered and unchangeable. Allows duplicate members

# Create Tuple
cars = ('Audi','BMW','Ferrari','Toyota','Tata','Lamborghini')

# Using Constructor
cars2 = tuple(('Maruti','Subaru','Lexus','Mercedes'))

print(cars,cars2)

# Leving  trailing , will treat the tuple as a string
cars3 = ('BMW')
print(cars3,type(cars3))

# But including trailing , will treat it as a tuple
cars4 = ('Bolero',)
print(cars4,type(cars4))

# Get a value from tuple ( Same as list )
print(cars[0])

# You can't change the value of tuple
# cars[0] = 'BMW'

# Delete tuple
del cars3
# print(cars3)

# Get length
print(len(cars))

# A Set is a collection which is unordered and unindexed. No duplicate members

# Create set
super_heroes = {'Batman','Spiderman','Superman','Ironman','Black Widow','Wonder woman'}

# Check if it is in set
print('Batman' in super_heroes)

# Add to set
super_heroes.add('Captain Marvel')
print(super_heroes)

# Remove from set
super_heroes.remove('Superman')
print(super_heroes)

# Add duplicate
super_heroes.add('Captain Marvel')
print(super_heroes)

# Clear set
super_heroes.clear()
print(super_heroes)

# Delete set
del super_heroes
print(super_heroes)