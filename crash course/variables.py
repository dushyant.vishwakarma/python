# This is a single-line comment

'''
This is a multi-line
comment or doctstring (used to define a function's puepose)
can be single or double quotes
'''

"""

    VARIABLE RULES : -
    - Variables name are case-sensitive (name and NAME are two different variables)
    - Must start with a letter and an underscore
    - Can have numbers but cannot start with one
"""

# x = 1                   # int
# y = 3.4                 # float
# name = 'John'           # str
# is_cool = True          # bool

# Multiple assignment
x, y , name, is_cool = (1,2.3,'John',True)

# print() is used to print output
print(x)
print(y,name,is_cool)

# Basic Math
sum = x + y
print(sum)

# Check the type of a variable
print(type(is_cool))

# Casting
x = str(x)
y = int(y)      
z = float(y)    

print(z)