# A List is a collection which can be ordered and can be changeable. Allows duplicate members also

# Create List
numbers = [1,2,3,4,5]
fruits = ['Apples','Oranges','Grapes','Banana']

# Using a Constructor
numbers2 = list((1,2,3,4,5))

# print(numbers,numbers2)

# Get a value from list
print(fruits[1])

# Get length of a list
print(len(fruits))

# Append to list
fruits.append('Mangos')

# Remove from list
fruits.remove('Grapes')

# Insert into position
fruits.insert(2,'Strawberries')

# Change value
fruits[0] = 'Blueberries'

# Remove with pop
fruits.pop(2)

# Reverse list
fruits.reverse()

# Sort list
fruits.sort()

# Reverse Sort
fruits.sort(reverse=True)

print(fruits)

