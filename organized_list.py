# Pythons sort() method makes it relatively easy to sort a list. Imagine we
# have a list of cars and want to change the order of the list to store them
# alphabetically. To keep the task simple, lets assume that all the values in
# the list are lowercase.

cars = ['BMW','Audi','Toyota','Maruti']

# To maintain the original order of a list but present it in a sorted order, you
# can use the sorted() function. The sorted() function lets you display your list
# in a particular order but doesn’t affect the actual order of the list.

print("Here is the original list of cars: ")
print(cars)

print("\nHere is the sorted list of cars: ")
print(sorted(cars))


# You can also sort this list in reverse alphabetical order by passing the
# argument reverse=True to the sort() method. The following example sorts
# the list of cars in reverse alphabetical order:

print("\nHere is the sorted list of cars in reverse: ")

cars.sort(reverse=True)
print(cars)

# To reverse the original order of a list, you can use the reverse() method.
# reverse() doesn’t sort backward alphabetically; it simply reverses the order of the list

sample = ['C','C++','C#','Python','Java']

print("\nHere is the sorted list of sample in reverse: ")
sample.reverse()
print(sample)

# You can quickly find the length of a list by using the len() function. The list
# in this example has four items, so its length is 4 :

print("\nLength of sample: ")
print(len(sample))




