# 4-3. Counting to Twenty: Use a for loop to print the numbers from 1 to 20,
# inclusive :

# for value in range(1,21):
#     print(value)

# 4-4. One Million: Make a list of the numbers from one to one million, and then
# use a for loop to print the numbers. (If the output is taking too long, stop it by
# pressing ctrl -C or by closing the output window.)

# for values in range(1,1000001):
#     print(values)

# 4-5. Summing a Million: Make a list of the numbers from one to one million,
# and then use min() and max() to make sure your list actually starts at one and
# ends at one million. Also, use the sum() function to see how quickly Python can
# add a million numbers.

one_mill = list(range(1,1000001))

print("Minimum from digits: " + str(min(one_mill)) + "\n")
print("Maximum from digits: " + str(max(one_mill)) + "\n")
print("Sum of digits: " + str(sum(one_mill)) + "\n")

# 4-6. Odd Numbers: Use the third argument of the range() function to make a list
# of the odd numbers from 1 to 20. Use a for loop to print each number.

odd_nums = list(range(1,21,2))
print(odd_nums)

# 4-7. Threes: Make a list of the multiples of 3 from 3 to 30. Use a for loop to
# print the numbers in your list.

multiples_of_three = list(range(3,31,3))
print(multiples_of_three)

# 4-8. Cubes: A number raised to the third power is called a cube. For example,
# the cube of 2 is written as 2**3 in Python. Make a list of the first 10 cubes (that
# is, the cube of each integer from 1 through 10), and use a for loop to print out
# the value of each cube.

# 4-9. Cube Comprehension: Use a list comprehension to generate a list of the
# first 10 cubes.

cubes = [value**3 for value in range(1,11)]
print(cubes)

# You can also work with a specific group of items in a list, which Python calls
# a slice.

# To make a slice, you specify the index of the first and last elements you
# want to work with. As with the range() function, Python stops one item
# before the second index you specify. To output the first three elements
# in a list, you would request indices 0 through 3 , which would return ele-
# ments 0 , 1 , and 2 .

players = ['john','mark','brad','sunny','rajesh']
print(players[0:3])
print(players[:1])
print(players[2:])

# you can output any slice from the end of a list. For example, if
# we want to output the last three players on the roster, we can use the slice
# players[-3:] :

print(players[-4:])


# You can use a slice in a for loop if you want to loop through a subset of
# the elements in a list. In the next example we loop through the first three
# players and print their names as part of a simple roster:

print("Here are the first four players on my team: ")

for player in players[:4]:
    print(player.title())

# To copy a list, you can make a slice that includes the entire original list
# by omitting the first index and the second index ( [:] ). This tells Python to
# make a slice that starts at the first item and ends with the last item, produc-
# ing a copy of the entire list.

my_foods = ['fried rice','manchurian','roti sabzi','pizza']
my_friends_food = my_foods[:]

print("\nMy Food: ")
print(my_foods)

print("\nMy Friends Food: ")
print(my_friends_food)

my_foods.append('chowmein')
my_friends_food.append('biryani')

print(my_foods)
print(my_friends_food)

# sometimes you’ll want to create a list of items that cannot
# change. Tuples allow you to do just that. Python refers to values that cannot
# change as immutable, and an immutable list is called a tuple.

# A tuple looks just like a list except you use parentheses instead of square
# brackets. Once you define a tuple, you can access individual elements by
# using each item’s index, just as you would for a list.

rectangel = (200,50)
print("\nLength of Rectangle: " + str(rectangel[0]))
print("Breadth of Rectangle: " + str(rectangel[1]))


# Although you can’t modify a tuple, you can assign a new value to a variable
# that holds a tuple. So if we wanted to change our dimensions, we could
# redefine the entire tuple:

rectangel = (400,50)
print("\nModified dimensions of Rectangle: ")
print("Length of Rectangle: " + str(rectangel[0]))
print("Breadth of Rectangle: " + str(rectangel[1]))