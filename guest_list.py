#  If you could invite anyone, living or deceased, to dinner, who
# would you invite Make a list that includes at least three people youd like to
# invite to dinner. Then use your list to print a message to each person, inviting
# them to dinner.

guest_list = ["Vikash","Rajesh","Harshit","Nishchay"]

print("You are invited " + guest_list[0])
print("You are invited " + guest_list[1])
print("You are invited " + guest_list[2])
print("You are invited " + guest_list[3])

print("\n\n")

print("Oops, " + guest_list[-1] + " cant make it.")
guest_list.pop(3)

print("\n\n")


guest_list.insert(3,"Manish")

print("You are invited " + guest_list[0])
print("You are invited " + guest_list[1])
print("You are invited " + guest_list[2])
print("You are invited " + guest_list[3])

print("\n\n")

print("I found out a bigger dinner table")

guest_list.insert(0,"Mansi")
guest_list.insert(2,"Karan")
guest_list.append("Vibhav")

print("\n\n")


print("You are invited " + guest_list[0])
print("You are invited " + guest_list[1])
print("You are invited " + guest_list[2])
print("You are invited " + guest_list[3])
print("You are invited " + guest_list[4])
print("You are invited " + guest_list[5])
print("You are invited " + guest_list[6])

print("\n\n")

print("I'm Sorry, dinner table won't arrive in time, and I have space for only two guests.")

print(guest_list)

print("\n\n")

guest_list.pop()
print("I'm Sorry Vibhav, I can't Invite you to the dinner.")
guest_list.pop()
print("I'm Sorry Manish, I can't Invite you to the dinner.")
guest_list.pop()
print("I'm Sorry Harshit, I can't Invite you to the dinner.")
guest_list.pop()
print("I'm Sorry Rajesh, I can't Invite you to the dinner.")
guest_list.pop()
print("I'm Sorry Karan, I can't Invite you to the dinner.")

print("\n\n")

print(guest_list[0] + " You are still Invited")
print(guest_list[1] + " You are still Invited")

print(guest_list)

del guest_list[0]
del guest_list[0]

print("\n\n")

print(guest_list)











